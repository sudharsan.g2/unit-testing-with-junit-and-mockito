package day2TestCase;

import static org.junit.Assert.*;

import org.junit.*;


public class AgeCheckerTest {
	
	static AgeChecker egc = null;
	
	@Before
	public void before() {
		 egc = new AgeChecker();
	}
	
	@After
	public void after() {
		System.out.println("@After");
	}
	@Test
	public void test1() {
		Object out1 = egc.voter("Alex",34);
		assertEquals(true, true);
		System.out.println("Test 1 Passed");
	}
	
	@Test
	public void test2() {
		Object out2 = egc.voter("Rani", 18);
		assertTrue(true);
		System.out.println("Test 2 Passed");

	}
	
	@Test
	public void test3() {
		Object out3 = egc.voter("Priya", 15);
		assertEquals(false, false);
		System.out.println("Test 3 Passed");

	}
	
	@Test
	public void test4() {
		Object out4 = egc.voter("Shan",50);
		assertFalse(false);
		System.out.println("Test 4 passed");
	}

}
