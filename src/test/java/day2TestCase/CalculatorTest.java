package day2TestCase;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.mockito.Mockito;

public class CalculatorTest {
	
	Calculator cal = null;
	/* stub
	CalculatorService service = new CalculatorService() {
		
		public int add(int i, int j) {
			// TODO Auto-generated method stub
			return 0;
		}
	};
	*/
	// mockito
	CalculatorService service = Mockito.mock(CalculatorService.class);
	@Before
	public void setUp() {
		cal =  new Calculator(service);
	}
	@Test
	public void testPerform() {
		
		when(service.add(2, 3)).thenReturn(5);
		assertEquals(10 ,cal.perform(2,3));
		verify(service).add(2, 3);
	}

}
